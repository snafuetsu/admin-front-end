﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AdminFrontEnd.DatabaseContext;

namespace AdminFrontEnd.Models.Repositories
{
    public class DbAdminsRepository : IAdminRepository
    {
        private AdminDbContext _db;

        public DbAdminsRepository(AdminDbContext db)
        {
            _db = db;
        }

        public Admin Create(Admin Admin)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public Admin Read(int id)
        {
            throw new NotImplementedException();
        }

        public ICollection<Admin> ReadAll()
        {
            throw new NotImplementedException();
        }

        public void Update(int id, Admin Admin)
        {
            throw new NotImplementedException();
        }
    }
}
