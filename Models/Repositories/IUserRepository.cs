﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminFrontEnd.Models.Repositories
{
    public interface IAdminRepository
    {
        public Admin Create(Admin Admin);
        public Admin Read(int id);
        public ICollection<Admin> ReadAll();
        public void Update(int id, Admin Admin);
        public void Delete(int id);
    }
}
