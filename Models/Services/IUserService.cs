﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminFrontEnd.Models.Services
{
    public interface IAdminService
    {
        Admin Authenticate(string Adminname, string password);
        IEnumerable<Admin> GetAll();
    }
}
