﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using AdminFrontEnd.Models;
using AdminFrontEnd.Models.Repositories;
using AdminFrontEnd.Models.Services;

namespace AdminFrontEnd.Controllers
{
    public class AdminController : Controller
    {
        private IAdminRepository _repo;
        private IAdminService _AdminService;

        public AdminController(IAdminRepository repo, IAdminService AdminService)
        {
            _repo = repo;
            _AdminService = AdminService;
        }

        public IActionResult Index()
        {
            var model = _repo.ReadAll();
            return View(model);
        }

        public IActionResult Login()
        {
            return View();
        }

        public IActionResult Register()
        {
            return View();
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(Admin Admin)
        {
            if (ModelState.IsValid)
            {
                _repo.Create(Admin);
                return RedirectToAction("Index");
            }
            return View(Admin);
        }

        public IActionResult Edit(int id)
        {
            var Admin = _repo.Read(id);
            if (Admin == null)
            {
                return RedirectToAction("Index");
            }
            return View(Admin);
        }

        [HttpPost]
        public IActionResult Edit(Models.Admin Admin)
        {
            if (ModelState.IsValid)
            {
                _repo.Update(Admin.Id, Admin);
                return RedirectToAction("Index");
            }
            return View(Admin);
        }

        public IActionResult Details(int id)
        {
            var Admin = _repo.Read(id);
            if (Admin == null)
            {
                return RedirectToAction("Index");
            }
            return View(Admin);
        }

        public IActionResult Delete(int id)
        {
            var Admin = _repo.Read(id);
            if (Admin == null)
            {
                return RedirectToAction("Index");
            }
            return View(Admin);
        }

        [HttpPost, ActionName("Delete")]
        public IActionResult DeleteConfirmed(int id)
        {
            _repo.Delete(id);
            return RedirectToAction("Index");
        }

        [AllowAnonymous]
        [HttpPost("authenticate")]
        public IActionResult Authenticate([FromBody]Admin AdminParam)
        {
            var Admin = _AdminService.Authenticate(AdminParam.AdminName, AdminParam.Password);

            if(Admin == null)
            {
                return BadRequest(new { message = "Adminname or password is incorrect" });
            }

            return Ok(Admin);
        }

        [HttpPost]
        public JsonResult PostAdmin(Admin Admin)
        {
            Debug.Assert(Admin != null);
            return Json(Admin);
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var Admins = _AdminService.GetAll();
            return Ok(Admins);
        }
       
    }
}