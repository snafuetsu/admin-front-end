﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminFrontEnd.Models.Services
{
    public class AppConfig
    {
        public string Secret { get; set; }
    }
}
