﻿"use strict";
//var category = "business";

/*
 * purpose: the ready function for the document
 * 
 * author: Taylor Cox
 */
$(document).ready(function () {
    /*var country = "us";
    var category = "business";
    var apiKey = "4e79c613dd4944509135c764a944d537"
    var inURL = generateURL(country, category, apiKey);*/

    var page = "1";
    var pagesize = "3";

    var inURL = generateURL(page, pagesize);



    /*
     * purpose: a function to generate a url 
     * 
     * author: Taylor Cox
     */
    function generateURL(page, pagesize) {
        var url = "http://metricapi-dev.us-west-2.elasticbeanstalk.com/api/Project?page="
                                                          + page + "&pagesize=" + pagesize;
        return url;
    }

    function getPage(inURL) {
        $.ajax({
            url: inURL,
            beforeSend: function () {
                $("#outputArea").html('<img src="' + '/images/ajax-loader.gif' + '">'); //displays this before sending the ajax call
            },
            success: function (response) {
                response.data.forEach(project => {

                    //displays the name of the project 
                    $('#name').html(project.name);

                    //displays project description
                    $('#description').html(project.description);
                });
            },
            error: function _error() {
                //inserts an error message into the element with id projectRows
                $('#projectRows').html('Project not found');
            }
        });
    }
})