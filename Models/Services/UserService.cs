﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;

namespace AdminFrontEnd.Models.Services
{
    public class AdminService : IAdminService
    {
        private AppConfig _appConfig;

        public AdminService(IOptions<AppConfig> appSettings)
        {
            _appConfig = appSettings.Value;
        }

        private List<Admin> _Admins = new List<Admin>
        {
            new Admin { Id = 1, AdminName = "test", Password = "test" }
        };

        public Admin Authenticate(string Adminname, string password)
        {
            var Admin = _Admins.SingleOrDefault(x => x.AdminName == Adminname && x.Password == password);

            // return null if Admin not found
            if (Admin == null)
                return null;

            // authentication successful so generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appConfig.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, Admin.Id.ToString())
                }),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            Admin.Token = tokenHandler.WriteToken(token);

            // remove password before returning
            Admin.Password = null;

            return Admin;
        }

        public IEnumerable<Admin> GetAll()
        {
            return _Admins.Select(x => {
                x.Password = null;
                return x;
            });
        }
    }
}
