﻿var sample = {};

sample.postData = function () {
    $.ajax({
        type: "POST", URL: "@Url.Action(\"Index\")",
        success: function (data) { alert('data: ' + data); },
        data: { "AdminName": "JohnDoe1", "Email": "JohnDoe1@gmail.com"},
        accept: 'application/json'
    });
};
document.ready(function () {
    sample.postData();
});